#Print the result of these operations:
print("Exercise 1:")
u,v,x,y,z =29,12,10,4,3
print("u / v =", round(u/v,2))
print("t = (u==v) -> t =", u==v)
print("u % v =", u%v)
print("t = (x>=y) -> t =", x>=y)
u+=5;
print("u+=5 -> u =", u)
u%=z
print("With new u, if u%=z -> u =", u)
print("t = (v > x and y < z)  ⇒ t =", v > x & y < z)
print("x ^ z =", x**z)
print("x // z =", x//z)

#Give string s, write script:
print()
print("Exercise 2:")
s = "Hi John, welcome to python programming for beginner!";
print(s)
if "python" in s:
    print("Yes!")
else:
    print("No!")

s1 = s[3:7];
print()
print("s1 =",s1)

print()
print("How many words are there in s?")
print("There are",len(s.split()), "words in s.")

#Compute the perimeter and area of a circle
print()
print("Exercise 3:")
def pa(r):
    print("Radius =",r)
    print("Perimeter =", round(2*3.24*r,3))
    print("Area =",round(3.14*3.14*r,3))

pa(5)

#Twinkle little star
print()
print("Exercise 4:")
print("Twinkle, twinkle, little star,")
print("{:>33}".format("How I wonder what you are!"))
print("Up above the world so high,")
print("{:>33}".format("Like a diamond in the sky."))
print("Twinkle, twinkle, little star,")
print("{:>33}".format("How I wonder what you are!"))

#List L
print()
print("Exercise 5:")
L = [23,4.3,4.2,31,"python",1,5.3,9,1.7]
print("Original L =", L)
L.remove("python")
L.sort(reverse=True)
print("New L =", L)
if 4.2 in L:
    print("4.2 is in L.")
else:
    print("4.2 is not in L.")